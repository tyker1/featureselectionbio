from __future__ import print_function
import os
import neat
import visualize
import pickle

import dataGenerator as dG
import mDataSet as mDSet
import numpy as np

import random

INT_NUM_VARIABLES = 100
INT_NUM_CORRELATION_PER_VAR = 20
INT_NUM_SAMPLES = 100
INT_RANDOM_SEED = 123
INT_TOTALVARIABLE = INT_NUM_CORRELATION_PER_VAR * INT_NUM_VARIABLES
CONFIG_PATH = "./neatConfig/FS_NEAT_default.ini"
OUTPUT_PATH = "./neat_fs_nohidden_alone.p"
REAL_DATA_PATH = "./alon_data.xlsx"
SHEET_NAME = "alon_data"
MAX_GENERATION = 300 # go through 10 generations
REC_GENERATION = 30 # it should make a checkpoint every 10 generation
CHECKPOINT = 10
# flag for training or evaluating
TRAINING = True
MULTITHREADING = True
MAXMIZING = True
CLASSIFY_BOOL = True
USE_YOUDEN = True
USE_REALDATA = True
CORES = 2
# generate the dataSet
mGenerator = dG.TDataSource(iSeed=INT_RANDOM_SEED, iNumVar=INT_NUM_VARIABLES,
                               iNumCorr=INT_NUM_CORRELATION_PER_VAR, iNumFeatures=INT_NUM_SAMPLES)
classification, samples = mGenerator.generateDataset()
if USE_REALDATA:
    # read from real data
    dataSet = mDSet.TDataSet.fromXLXSFile(REAL_DATA_PATH, SHEET_NAME)
    dataSet.splitTrainTestData(testSize=0.5)
    inputs = dataSet.sampleTrain
    outputs = dataSet.classTrain
    testInputs = dataSet.sampleTest
    testOutputs = dataSet.classTest
    INT_NUM_SAMPLES = len(inputs[:,0])
else:
    # set data manager
    dataSet = mDSet.TDataSet(dataSamples=samples, classifications=classification, RANDOM_SEED=INT_RANDOM_SEED)
    # scale it (normalize)
    dataSet.scaleDataSet()
    inputs = dataSet.samples
    outputs = dataSet.classifications

if not CLASSIFY_BOOL:
    outputs = list(map(int,outputs))
    
print("Total Positive Sample =%04d"%(sum(outputs)))
random.seed(INT_RANDOM_SEED)

def eval_genomes(genomes, config):
    # iterate through all individuals
    for genome_id, genome in genomes:
        # print("Evaluating Genome_%02d in the population"%genome_id)
        # assume that it classifies all sample correctly
        if MAXMIZING:
            correctClassification = INT_NUM_SAMPLES
        else:
            correctClassification = 0
            
        falsePositiv = 0
        falseNegativ = 0   
        truePositive = 0
        trueNegative = 0
        # generate test series
        idxs = random.sample(range(0,INT_NUM_SAMPLES), INT_NUM_SAMPLES)
        # get test data
        net = neat.nn.FeedForwardNetwork.create(genome=genome, config=config)
        for i in idxs:
            output = net.activate(inputs[i,:])
            result = False
            if (output[0] > 0.5):
                result = True
            if (result != outputs[i]):
                correctClassification -= 1
            
            if (outputs[i] and (not result)):
                falseNegativ += 1
            if ((not outputs[i]) and result):
                falsePositiv += 1
            if (outputs[i] and result):
                truePositive += 1
            if ((not outputs[i]) and (not result)):
                trueNegative += 1
            # if MAXMIZING:
            #     correctClassification -= (output[0] - outputs[i])**2
            # else:
            #     correctClassification += (output[0] - outputs[i])**2
        # assign the rate of correct classification as fitness
        if correctClassification < 0:
            correctClassification = 0
        youden = (truePositive)/(truePositive+falseNegativ) + (trueNegative)/(trueNegative+falsePositiv) - 1
        # genome.fitness = correctClassification * 1.0 / INT_NUM_SAMPLES
        if MAXMIZING:
            fitness = correctClassification * 1.0 / INT_NUM_SAMPLES
        else:
            fitness = correctClassification
        if USE_YOUDEN:
            genome.fitness = youden
        else:
            genome.fitness = fitness
        # print("Genome No.%02d resulted with %.04f correct classifications"%(genome_id, genome.fitness))

def eval_genome(genome, config):
    # assume that it classifies all sample correctly
    if MAXMIZING:
            correctClassification = INT_NUM_SAMPLES
    else:
            correctClassification = 0
    # initialize for calculating the youden index
    falsePositiv = 0
    falseNegativ = 0   
    truePositive = 0
    trueNegative = 0
    
    # generate test series
    idxs = random.sample(range(0,INT_NUM_SAMPLES), INT_NUM_SAMPLES)
    # get test data
    net = neat.nn.FeedForwardNetwork.create(genome=genome, config=config)
    for i in idxs:
        output = net.activate(inputs[i,:])
        result = False
        if (output[0] > 0.5):
            result = True
        if (result != outputs[i]):
            correctClassification -= 1
            
        if (outputs[i] and (not result)):
            falseNegativ += 1
        if ((not outputs[i]) and result):
            falsePositiv += 1
        if (outputs[i] and result):
            truePositive += 1
        if ((not outputs[i]) and (not result)):
            trueNegative += 1
        # if MAXMIZING:
        #     correctClassification -= (output[0] - outputs[i])**2
        # else:
        #     correctClassification += (output[0] - outputs[i])**2
    # assign the rate of correct classification as fitness
    
    youden = (truePositive)/(truePositive+falseNegativ) + (trueNegative)/(trueNegative+falsePositiv) - 1
    if correctClassification < 0:
        correctClassification = 0
    if MAXMIZING:
        fitness = correctClassification * 1.0 / INT_NUM_SAMPLES
    else:
        fitness = correctClassification
    if USE_YOUDEN:
        return youden
    else:
        return fitness
    # print("Genome No.%02d resulted with %.04f correct classifications"%(genome_id, genome.fitness))

def run():
    config = neat.Config(neat.DefaultGenome, neat.DefaultReproduction,
                         neat.DefaultSpeciesSet, neat.DefaultStagnation, CONFIG_PATH)
    pop = neat.Population(config)

    # recode history
    stats = neat.StatisticsReporter()
    pop.add_reporter(stats)
    pop.add_reporter(neat.StdOutReporter(True))
    # it should make a checkpoint every REC_GENERATION or every hour
    pop.add_reporter(neat.Checkpointer(generation_interval=REC_GENERATION, time_interval_seconds=3600))
    if MULTITHREADING :
        pe = neat.ParallelEvaluator(CORES, eval_genome)
    # train for MAX_GENERATIONs
        winner = pop.run(pe.evaluate, MAX_GENERATION)
    else:
        winner = pop.run(eval_genomes, MAX_GENERATION)
    outputFile = open(OUTPUT_PATH,'wb' )
    pickle.dump(winner, outputFile)
    
    visualize.plot_stats(stats, ylog=False, view=True)
    visualize.plot_species(stats, view=True)
    node_names = {0:"Out"}
    for i in range(-1, int(-1 * INT_NUM_VARIABLES), -1):
        node_names[i] = "In%03d"%(-1*i)
    # visualize the net, hide unused nodes but show disabled connections
    visualize.draw_net_noRedundant(config, winner, view=True, show_disabled=True, node_names=node_names)
    # Display the winning genome.
    print('\nBest genome:\n{!s}'.format(winner))
    
    # test the winning genome
    net = neat.nn.FeedForwardNetwork.create(winner, config)
    if not USE_REALDATA:
        classification, samples = mGenerator.generateDataset(1000)
        newDataSet = mDSet.TDataSet(dataSamples=samples, classifications=classification, RANDOM_SEED=INT_RANDOM_SEED)
        newDataSet.scaleDataSet()
        testInput = newDataSet.samples
        testOutput = newDataSet.classifications
        missClassification = 0
        # initialize for calculating the youden index
        falsePositiv = 0
        falseNegativ = 0   
        truePositive = 0
        trueNegative = 0
        for i in range(0, 1000):
            output = net.activate(testInput[i,:])
            result = False
            if output[0] > 0.5:
                result = True
            if (result != classification[i]):
                missClassification += 1
            if (classification[i] and (not result)):
                falseNegativ += 1
            if ((not classification[i]) and result):
                falsePositiv += 1
            if (classification[i] and result):
                truePositive += 1
            if ((not classification[i]) and (not result)):
                trueNegative += 1
    else:
        missClassification = 0
        # initialize for calculating the youden index
        falsePositiv = 0
        falseNegativ = 0   
        truePositive = 0
        trueNegative = 0
        for i in range(0, len(testInputs[:,0])):
            output = net.activate(testInputs[i,:])
            result = False
            if output[0] > 0.5:
                result = True
            if (result != testOutputs[i]):
                missClassification += 1
            if (testOutputs[i] and (not result)):
                falseNegativ += 1
            if ((not testOutputs[i]) and result):
                falsePositiv += 1
            if (testOutputs[i] and result):
                truePositive += 1
            if ((not testOutputs[i]) and (not result)):
                trueNegative += 1

    print("Evaluation result:")
    print("Total Sample Tested        = 1000")
    print("Missclassifications        = %04d"%missClassification)
    print("True Positive =%d\t True Negative =%d"%(truePositive, trueNegative))
    print("False Positive=%d\t False Negative=%d"%(falsePositiv, falseNegativ))
    print("Rate of Missclassification = %.4f"%(missClassification/1000))
    
def evaluation():
    p = neat.Checkpointer.restore_checkpoint('neat-checkpoint-%i' % CHECKPOINT)
    # just get the best result from that checkpoint
    winner = p.run(eval_genomes, 1)
    
    # assign name to input and output nodes
    node_names = {0:"Out"}
    for i in range(-1, int(-1 * INT_NUM_VARIABLES), -1):
        node_names[i] = "In%03d"%(-1*i)
    # visualize the net, hide unused nodes but show disabled connections
    visualize.draw_net_noRedundant(p.config, winner, view=True, show_disabled=True, node_names=node_names)
    
    net = neat.nn.FeedForwardNetwork.create(winner, p.config)
    classification, samples = mGenerator.generateDataset(1000)
    missClassification = 0
    for i in range(0, 1000):
        output = net.activate(samples[i,:])
        if (output != classification[i]):
            missClassification += 1
    
    print("Evaluation result:")
    print("Total Sample Tested        = 1000")
    print("Missclassifications        = %05d"%missClassification)
    print("Rate of Missclassification = %.3f"%(missClassification/1000))
    
    
if __name__ == '__main__':
    if TRAINING:
        run()
    else:
        evaluation()
    
    