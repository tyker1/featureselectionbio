from __future__ import print_function
import os
import neat
import visualize
import pickle

import dataGenerator as dG
import mDataSet as mDSet
import numpy as np

import random

INT_NUM_VARIABLES = 100
INT_NUM_CORRELATION_PER_VAR = 20
INT_NUM_SAMPLES = 200
INT_RANDOM_SEED = None
CHECK_SEED = 123
CHECK_SAMPLES = 100
INT_TOTALVARIABLE = INT_NUM_CORRELATION_PER_VAR * INT_NUM_VARIABLES
CONFIG_PATH = "./neatConfig/FS_NEAT_default.ini"
GENOME_PATH = "./neat_result/fs_nohidden_1.0_0.5_real_5050Test/neat_fs_nohidden_alone.p"
CHECKPOINT_FORMAT = "neat-checkpoint-%d"
REAL_DATA_PATH = "./alon_data.xlsx"
SHEET_NAME = "alon_data"
CHECKPOINT = 59
MAX_GENERATION = 10 # go through 10 generations
REC_GENERATION = 10 # it should make a checkpoint every 10 generation
CHECKPOINT = 10
# flag for training or evaluating
TRAINING = True
MULTITHREADING = False
MAXMIZING= True
USE_YOUDEN = True
USE_REALDATA = True
mGenerator = dG.TDataSource(iSeed=INT_RANDOM_SEED, iNumVar=INT_NUM_VARIABLES,
                               iNumCorr=INT_NUM_CORRELATION_PER_VAR, iNumFeatures=INT_NUM_SAMPLES)
checkGenerator = dG.TDataSource(iSeed=CHECK_SEED, iNumVar=INT_NUM_VARIABLES,
                               iNumCorr=INT_NUM_CORRELATION_PER_VAR, iNumFeatures=CHECK_SAMPLES)
classification, samples = mGenerator.generateDataset()
checkClassification, checkSamples = checkGenerator.generateDataset()
# set data manager
if USE_REALDATA:
    dataSet = mDSet.TDataSet.fromXLXSFile(REAL_DATA_PATH, SHEET_NAME)
    INT_NUM_SAMPLES = len(dataSet.samples[:,0])
else:
    dataSet = mDSet.TDataSet(dataSamples=samples, classifications=classification, RANDOM_SEED=INT_RANDOM_SEED)
checkDataSet = mDSet.TDataSet(dataSamples=checkSamples, classifications=checkClassification, RANDOM_SEED=CHECK_SEED)
# scale it (normalize)
dataSet.scaleDataSet()
checkDataSet.scaleDataSet()
checkInputs = checkDataSet.samples
checkOutputs = checkDataSet.classifications
inputs = dataSet.samples
outputs = dataSet.classifications
print("Total Positive Sample =%04d"%(sum(classification)))
random.seed(INT_RANDOM_SEED)

def eval_genomes(genomes, config):
    # iterate through all individuals
    for genome_id, genome in genomes:
        # print("Evaluating Genome_%02d in the population"%genome_id)
        # assume that it classifies all sample correctly
        if MAXMIZING:
            correctClassification = INT_NUM_SAMPLES
        else:
            correctClassification = 0
            
        falsePositiv = 0
        falseNegativ = 0   
        truePositive = 0
        trueNegative = 0
        # generate test series
        idxs = random.sample(range(0,INT_NUM_SAMPLES), INT_NUM_SAMPLES)
        # get test data
        net = neat.nn.FeedForwardNetwork.create(genome=genome, config=config)
        for i in idxs:
            output = net.activate(checkInputs[i,:])
            result = False
            if (output[0] > 0.5):
                result = True
            if (result != checkOutputs[i]):
                correctClassification -= 1
            
            if (checkOutputs[i] and (not result)):
                falseNegativ += 1
            if ((not checkOutputs[i]) and result):
                falsePositiv += 1
            if (checkOutputs[i] and result):
                truePositive += 1
            if ((not checkOutputs[i]) and (not result)):
                trueNegative += 1
        # assign the rate of correct classification as fitness
        if correctClassification < 0:
            correctClassification = 0
        youden = (truePositive)/(truePositive+falseNegativ) + (trueNegative)/(trueNegative+falsePositiv) - 1
        # genome.fitness = correctClassification * 1.0 / INT_NUM_SAMPLES
        if MAXMIZING:
            fitness = correctClassification * 1.0 / INT_NUM_SAMPLES
        else:
            fitness = correctClassification
        if USE_YOUDEN:
            genome.fitness = youden
        else:
            genome.fitness = fitness

def test_genome(genome, config):
    # assume that it classifies all sample correctly
    correctClassification = 0
    # generate test series
    idxs = random.sample(range(0,INT_NUM_SAMPLES), INT_NUM_SAMPLES)
    # get test data
    net = neat.nn.FeedForwardNetwork.create(genome=genome, config=config)
    falsePositiv = 0
    falseNegativ = 0
    
    truePositive = 0
    trueNegative = 0
    for i in idxs:
        output = net.activate(inputs[i,:])
        result = False
        if (output[0] > 0.5):
            result = True
        if (outputs[i] == result):
            correctClassification += 1
            
        if (outputs[i] and (not result)):
            falseNegativ += 1
        if ((not outputs[i]) and result):
            falsePositiv += 1
        if (outputs[i] and result):
            truePositive += 1
        if ((not outputs[i]) and (not result)):
            trueNegative += 1
    print("True Positive =%d\tTrue Negative =%d"%(truePositive, trueNegative))
    print("False Positive=%d\tFalse Negative=%d"%(falsePositiv,falseNegativ))
    print(net.activate(inputs[0,:]))
    return correctClassification

if __name__ == '__main__':
    config = neat.Config(neat.DefaultGenome, neat.DefaultReproduction,
                         neat.DefaultSpeciesSet, neat.DefaultStagnation,
                         CONFIG_PATH)
    genome = pickle.load(open(GENOME_PATH, 'rb'))
    fitness = test_genome(genome, config)
    print('\nBest genome:\n{!s}'.format(genome))
    # assign name to input and output nodes
    node_names = {0:"Out"}
    net = neat.nn.FeedForwardNetwork.create(genome, config)
    if USE_REALDATA:
        for i in range(-1, int(-1 * INT_TOTALVARIABLE), -1):
            node_names[i] = dataSet.colNames[i]
    else:
        for i in range(-1, int(-1 * INT_TOTALVARIABLE), -1):
            node_names[i] = "In%03d"%(-1*i)
    # visualize the net, hide unused nodes but show disabled connections
    visualize.draw_net_noRedundant(config, genome, filename="neat_fs_nohidden_net", view=True, show_disabled=True, node_names=node_names, fmt='png')
    print("Total %d correct classifications in %d Data samlpe"%(fitness,INT_NUM_SAMPLES))
    pass