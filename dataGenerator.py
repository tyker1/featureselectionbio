import numpy as np
import random
import pandas as pd
from scipy.linalg import cholesky
from scipy.stats import pearsonr

class TDataSource(object):
    def __init__(self, iSeed, iNumVar, iNumCorr, iNumFeatures):
        self.__iSeed = iSeed
        self.__iNumVar = iNumVar
        self.__iNumCorr = iNumCorr
        self.__iNumFeatures = iNumFeatures
        self.relativFeatures = None
        random.seed(self.__iSeed)
        np.random.seed(self.__iSeed)

    def generateSemiPositiveDefiniteCorrMatrix(self):
        tempMat = np.eye(self.__iNumCorr, dtype="float")
        tempMat = np.random.rand(self.__iNumCorr, self.__iNumCorr) * 2 - 1
        # for j in range(1, self.__iNumCorr):
        #     correlation = np.random.rand(1, j) * 2 - 1
        #     tempMat[self.__iNumCorr - 1 - j, (self.__iNumCorr - j):] = correlation
        #     tempMat[(self.__iNumCorr - j):, self.__iNumCorr - 1 - j] = correlation
        return np.dot(tempMat, tempMat.transpose())

    def getCorrelationMatrix(self, dFeatures, iVariables):
        newCorrMat = np.eye(iVariables)
        for i in range(iVariables):
            for j in range(i, iVariables):
                corr, _ = pearsonr(dFeatures[:, i], dFeatures[:, j])
                newCorrMat[i, j] = corr
                newCorrMat[j, i] = corr
        return newCorrMat

    def generateCorrelatedFeatures(self, dCorrelationMatrix, iNumFeatures=None):
        if iNumFeatures is None:
            iNumFeatures = self.__iNumFeatures

        # Compute the (upper) Cholesky decomposition matrix
        upper_chol = cholesky(dCorrelationMatrix)

        # Generate randome Variables
        rnd = np.random.normal(0.0, 1.0, size=(iNumFeatures, self.__iNumCorr))
        dFeatures = rnd @ upper_chol

        return dFeatures

    def dummyClassification(self, dSample):
         counter = 0
         for i in range(0, self.__iNumCorr * self.__iNumVar - 1, self.__iNumCorr):
            if -0.1 <= dSample[i] <= 0.12:
                counter = counter + 1
        
         if counter > 3:
            return True
         else:
            return False
        #if self.relativFeatures is None:
        #    self.relativFeatures = self.selectRandomFeature()
        
        #for i in self.relativFeatures:
        #    if dSample[i] < 0:
        #        return False
        #return True
        
    def selectRandomFeature(self):
        if self.__iNumVar >= 4:
            lstVars = random.sample(range(0, self.__iNumVar*self.__iNumCorr, self.__iNumCorr),4)
            lstDiff = random.sample(range(0,self.__iNumCorr), 4)
        else:
            lstVars = range(0, self.__iNumVar*self.__iNumCorr, self.__iNumVar)
            lstDiff = random.sample(range(0,self.__iNumCorr), self.__iNumVar)
            
        return [sum(x) for x in zip(lstVars, lstDiff)]
        
    def generateDataset(self, iNumFeatures = None):
        if iNumFeatures is None:
            iNumFeatures = self.__iNumFeatures

        # generate Samples
        self.Var = None
        for i in range(self.__iNumVar):
            correlationMatrix = self.generateSemiPositiveDefiniteCorrMatrix()
            featureMat = self.generateCorrelatedFeatures(correlationMatrix, iNumFeatures)
            if self.Var is None:
                self.Var = featureMat
            else:
                self.Var = np.hstack((self.Var, featureMat))
        # Classify it
        if self.relativFeatures is None:
            self.relativFeatures = self.selectRandomFeature()
        self.result = []
        for i in range(iNumFeatures):
            self.result.append(self.dummyClassification(self.Var[i, :]))

        return self.result, self.Var

# mySeed = 123
#
# random.seed(mySeed)
#
# np.random.seed(mySeed)
#
# NUM_VARIABLES_HAS_COR = 30  # 30 variables have correlation with
# NUM_COR_VARIABLES = 100      # each of those variables are correlated with 100 random other variables
# NUM_VARIABLES = 2000        # a record should contain 2000 variables
#
# corrMat = np.eye(NUM_VARIABLES, dtype="float")  # generate the unit matrix for correlation matrix
#
# # get NUM_VARIABLES_HAS_COR variables randomly
# variable_selected = random.sample(range(NUM_VARIABLES), NUM_VARIABLES_HAS_COR)
#
# # loop through all selected samples and generate correlations for them with other NUM_COR_VARIABLES
# for main_var in variable_selected:
#     sub_variables_selected = random.sample(range(NUM_VARIABLES), NUM_COR_VARIABLES)
#     # regenerate if it already has correlations or it is itself or it is in the main correlation variables
#     while (main_var in sub_variables_selected) or (not corrMat[:, variable_selected].any):
#         sub_variables_selected = random.sample(range(NUM_VARIABLES), NUM_COR_VARIABLES)
#
#     # generate random correlations
#     correlation = np.random.rand(1, NUM_COR_VARIABLES) * 2 - 1
#     # fill the matrix
#     corrMat[main_var, sub_variables_selected] = correlation
#     corrMat[sub_variables_selected, main_var] = correlation
#
#
#
# df_corrMat = pd.DataFrame(corrMat)
# df_corrMat.to_csv('correlation'+str(mySeed)+'.csv')
#
#
# print(corrMat)
#
#
#
