# Feature Selection/Extraction for Highdimensional Dataset with low instance

This project yelds to find out a method for feature selection and/or feature extraction for Dataset with high dimensionality but low number of instances.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

This project is written in Python (version 3.7.4) with many dependend Packages if you are using linux, use the following commands to install them. If using windows, manually install python 3.7 and graphivz, make sure that they are in the system path, so the programm can find them. And use the pip relevant commands to install the packages

``` shell
sudo apt-get install python3.7
sudo apt-get install graphivz
pip install numpy
pip install pandas
pip install sklearn
pip install graphivz
pip install matplotlib
pip install deap
pip install neat-python
```

## Progress Histories

This Section will record all changes made and the progress made

### Tuesday, October 22, 2019

NEAT got tested, with the simulated data, result was very bad, it might be that the data is badlly generated, should **explizitly generate the relevant attribute** instead of randomly choosing them.

GA works however much better with the simulated data, but it turns out that it was used **again the test dataset to fit the KNN**, which is not **desired**.

FS-NEAT and GA tested with the *alon dataset* for more test samples, it the Dataset is divided into 2 Sets using leave-one-out method, with training and testing Set with same number of instances.
For GA testing, it would use the the KNN that is produced during evolution (for each individual, a new knn will be generated, and the knn that generated with the fittest individual will be used here)

**Test Results**

``` python
"""GA results"""
With total of 70 features for the best performance
With the total missclassification of 6 in total of 31 samples
Features selected:
['Hsa.13491', 'Hsa.24464', 'Hsa.2597', 'Hsa.3006', 'UMGAP.1', 'Hsa.98', 'Hsa.18664', 'Hsa.25451', 'Hsa.39141', 'Hsa.24948', 'Hsa.304', 'Hsa.41315', 'Hsa.891', 'Hsa.1276', 'Hsa.4347', 'Hsa.454', 'Hsa.244', 'Hsa.2086', 'Hsa.8583', 'Hsa.13670', 'Hsa.3069.1', 'Hsa.1244', 'Hsa.847', 'Hsa.24944', 'Hsa.318', 'Hsa.2179', 'Hsa.15167', 'Hsa.1799', 'Hsa.2978', 'Hsa.1676', 'Hsa.364', 'Hsa.1309', 'Hsa.1598', 'Hsa.27648', 'Hsa.3041', 'Hsa.3876', 'Hsa.688', 'Hsa.1127', 'Hsa.602', 'Hsa.14', 'Hsa.32358', 'Hsa.3969', 'Hsa.123', 'Hsa.1672', 'Hsa.25481', 'Hsa.1467', 'Hsa.2765', 'Hsa.8833', 'Hsa.1552', 'Hsa.2922', 'Hsa.1383', 'Hsa.2774', 'Hsa.5130', 'Hsa.3238', 'Hsa.9102', 'Hsa.1316', 'Hsa.14763', 'Hsa.6619', 'Hsa.2343', 'Hsa.960', 'Hsa.8121', 'Hsa.3015', 'Hsa.2137', 'Hsa.1194', 'Hsa.1660', 'Hsa.1607', 'Hsa.1016', 'Hsa.17213', 'Hsa.33012', 'Hsa.33']
```

``` python
"""FS-NEAT results"""
Best fitness: 1.00000 - size: (5, 10) - species 34 - id 129641

Best individual in generation 65 meets fitness threshold - complexity: (5, 10)

Best genome:
Key: 129641
Fitness: 1.0
Nodes:
        0 DefaultNodeGene(key=0, bias=-4.188973335533776, response=1.0, activation=sigmoid, aggregation=sum)
        7688 DefaultNodeGene(key=7688, bias=1.3719424444614259, response=1.0, activation=relu, aggregation=sum)
        29550 DefaultNodeGene(key=29550, bias=-1.1102972802010558, response=1.0, activation=sigmoid, aggregation=sum)
        44125 DefaultNodeGene(key=44125, bias=0.1580358896091184, response=1.0, activation=relu, aggregation=sum)
        49428 DefaultNodeGene(key=49428, bias=1.3447643750882228, response=1.0, activation=relu, aggregation=sum)
Connections:
        DefaultConnectionGene(key=(-1772, 7688), weight=1.9249920662131683, enabled=True)
        DefaultConnectionGene(key=(-1348, 7688), weight=-2.9553478682960783, enabled=True)
        DefaultConnectionGene(key=(-1299, 29550), weight=1.5278821140764554, enabled=True)
        DefaultConnectionGene(key=(-1221, 0), weight=1.6342967502672123, enabled=True)
        DefaultConnectionGene(key=(-1186, 0), weight=1.0745624191788994, enabled=True)
        DefaultConnectionGene(key=(-1186, 44125), weight=2.4487588354704783, enabled=True)
        DefaultConnectionGene(key=(-1186, 49428), weight=0.7186664426773689, enabled=True)
        DefaultConnectionGene(key=(7688, 0), weight=2.3218432110812137, enabled=True)
        DefaultConnectionGene(key=(44125, 0), weight=1.0, enabled=True)
        DefaultConnectionGene(key=(49428, 0), weight=0.944282744459491, enabled=True)
Evaluation result:
Total Sample Tested        = 31
Missclassifications        = 0006
True Positive =16        True Negative =9
False Positive=1         False Negative=5

```
**TODO**:

* create the simulated Dataset with explizitly generated Samples (the best is to make it like a 50-50 dataset, like 50% positive and 50% negative
* implement the Genetic Programming for the same Problem
* write the document

## Built With

* [Python3.7](https://www.python.org/downloads/release/python-374/) - The Python interpreter
* [Graphivz](https://graphviz.gitlab.io/download/) - Program for visualizing the neural networks
* [NEAT-Python](https://neat-python.readthedocs.io/en/latest/neat_overview.html) - NEAT-Python Documentation
* [DEAP](https://deap.readthedocs.io/en/master/) - Documentation for DEAP Package, used for GA and GP

## Version History

* *Tuesday, October 22, 2019* - Version 0.1.0

## Authors

* **Lewei He** - *Initial work* - [FeatureSelectionBio](https://bitbucket.org/tyker1/featureselectionbio/src/master/)
