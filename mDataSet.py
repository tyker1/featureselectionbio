import random
import numpy as np
import pandas as pd
from datetime import datetime
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler

class TDataSet(object):
    def __init__(self, dataSamples, classifications, RANDOM_SEED, colNames = None):
        self.samples = dataSamples
        self.classifications = classifications
        self.sampleTrain = None
        self.sampleTest = None
        self.classTrain = None
        self.classTest = None
        self.isScaled = False
        self.RANDOMSEED = RANDOM_SEED
        if colNames is None:
            self.colNames = []
        else:
            self.colNames = colNames
        
    @classmethod
    def fromXLXSFile(cls, fileName, sheetName):
        df = pd.read_excel(fileName, sheet_name=sheetName)
        data = df.values
        tempClassification = data[:,0]
        classifications = list(map(lambda x: 't' == x, tempClassification))
        samples = data[:,1:]
        return cls(samples, classifications, None, list(df.columns))
        
    def scaleDataSet(self):
        if self.isScaled:
            return

        scaler = StandardScaler()
        scaler.fit(self.samples)

        self.samples= scaler.transform(self.samples)
        # self.sampleTest = scaler.transform(self.sampleTest)

        self.isScaled = True

    def splitTrainTestData(self, testSize=0.2):
        if not self.isScaled:
            self.scaleDataSet()

        sampleTrain, sampleTest, classTrain, classTest = train_test_split(self.samples, self.classifications,
                                                                          test_size=testSize, random_state=self.RANDOMSEED)
        self.sampleTrain = sampleTrain
        self.sampleTest = sampleTest
        self.classTrain = classTrain
        self.classTest = classTest

    def getTrainData(self):
        return self.sampleTrain, self.classTrain

    def getTestData(self):
        return self.sampleTest, self.classTest

    def selectData(self, idx):
        sTrain = self.sampleTrain[:, idx]
        sTest = self.sampleTest[:, idx]
        cTrain = self.classTrain
        cTest = self.classTest
        return sTrain, sTest, cTrain, cTest

    def selectDataNoSplit(self, idx):
        dtSamples = self.samples[:, idx]
        dsClassification = self.classifications
        return dtSamples, dsClassification