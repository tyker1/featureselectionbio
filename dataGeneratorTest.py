import unittest
import dataGenerator as dGenerator
from scipy.stats import pearsonr
import numpy as np
import numpy.testing as npt


class MyTestCase(unittest.TestCase):
    def test_feature(self):
        print("Test Correlation Matrix")
        generator = dGenerator.TDataSource(iSeed=123, iNumVar=3, iNumCorr=5, iNumFeatures=500)
        positiveDefinite = generator.generateSemiPositiveDefiniteCorrMatrix()
        features = generator.generateCorrelatedFeatures(dCorrelationMatrix=positiveDefinite)
        correlationMatrix = generator.getCorrelationMatrix(features, iVariables=5)
        newCorrMat = np.eye(5)
        for i in range(5):
            for j in range(i, 5):
                corr, _ = pearsonr(features[:, i], features[:, j])
                newCorrMat[i, j] = corr
                newCorrMat[j, i] = corr

        npt.assert_array_almost_equal(correlationMatrix, newCorrMat)

    def test_featureGeneration(self):
        print("Test Feature Generation")
        generator = dGenerator.TDataSource(iSeed=123, iNumVar=100, iNumCorr=20, iNumFeatures=10000)
        classification, features = generator.generateDataset()
        mSize = np.shape(features)
        classes = sum(classification)
        
        self.assertEqual(classes, 10000)
        self.assertSequenceEqual(mSize, (10000, 2000))


if __name__ == '__main__':
    unittest.main()
