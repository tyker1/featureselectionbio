import random
import numpy as np

import dataGenerator as dG
import mDataSet as DS

from deap import base
from deap import creator
from deap import tools
from deap import algorithms

from sklearn.neighbors import KNeighborsClassifier
from sklearn.metrics import classification_report, confusion_matrix

from scoop import futures

INT_NUM_VARIABLES = 100
INT_NUM_CORRELATION_PER_VAR = 20
INT_NUM_SAMPLES = 62
INT_RANDOM_SEED = 123
INT_TOTALVARIABLE = INT_NUM_CORRELATION_PER_VAR * INT_NUM_VARIABLES
INT_MAX_SEARCH_K = int(INT_NUM_CORRELATION_PER_VAR/2)
globalNumCore = 0
globalBestJ = 0
NGEN = 100 # maximun generation
POPULATION_SIZE = 100
CROSSOVER_PROBABILITY = 0.3
MUTATE_PROBABILITY = 0.2
ALPHA = 0.99

DATA_FILE_NAME = 'alon_data.xlsx'
DATA_SHEET_NAME = 'alon_data'
REALDATA = True

testDataSet = None
knnClassi = None
def randBool(dProbability):
    return random.random() < dProbability

def rateCorrectClassification(lstAttr, _dataSet):
    global globalBestJ
    global globalNumCore
    global knnClassi
    idx = np.flatnonzero(lstAttr)
    _sampleTrain, _sampleTest, _classTrain, _classTest = _dataSet.selectData(idx)
    Jcoeff = 0
    # for i in range(1, INT_MAX_SEARCH_K):
    knn = KNeighborsClassifier(n_neighbors=10)
    knn.fit(_sampleTrain, _classTrain)
    pred_i = knn.predict(_sampleTest)
    missClasification = sum(i != j for i,j in zip(pred_i, _classTest))
    _J = 1 - 1.0*missClasification/len(pred_i)	
    if _J > Jcoeff:
        Jcoeff = _J
        if globalBestJ < Jcoeff:
            globalBestJ = Jcoeff
            globalNumCore = 3
    del knn
    return missClasification
    pass

def fitTraindata(lstAttr, _dataSet):
    idx = np.flatnonzero(lstAttr)
    _sampleTrain, _sampleTest, _classTrain, _classTest = _dataSet.selectData(idx)
    knn = KNeighborsClassifier(n_neighbors=10)
    knn.fit(_sampleTrain, _classTrain)
    
    return knn

def varifyAttributes(lstAttr, _dataSet, knn):
    idx = np.flatnonzero(lstAttr)
    _samples, _classifications = _dataSet.selectDataNoSplit(idx)
    
    pred_i = knn.predict(_samples)
    missClasification = sum(i != j for i,j in zip(pred_i, _classifications))
    
    return missClasification


def evaluate(individual, _dataSet):
    
    youdenIndex = rateCorrectClassification(individual, _dataSet)
    numAttr = sum(individual)/INT_TOTALVARIABLE
    # print("Individual evaluated, Fitness=%f"%youdenIndex)
    fitness = ALPHA*youdenIndex + (1-ALPHA) * numAttr / INT_TOTALVARIABLE
    # return youdenIndex, numAttr
    return fitness,

def GeneticAlgorithmWithProgress(population, toolbox, cxpb, mutpb, ngen, stats=None,
             halloffame=None, verbose=__debug__):
    
    """This Function is copied from DEAP Library and modified 
    to output progress each generation"""
    
    logbook = tools.Logbook()
    logbook.header = ['gen', 'nevals'] + (stats.fields if stats else [])

    # Evaluate the individuals with an invalid fitness
    invalid_ind = [ind for ind in population if not ind.fitness.valid]
    fitnesses = toolbox.map(toolbox.evaluate, invalid_ind)
    for ind, fit in zip(invalid_ind, fitnesses):
        ind.fitness.values = fit

    if halloffame is not None:
        halloffame.update(population)

    record = stats.compile(population) if stats else {}
    logbook.record(gen=0, nevals=len(invalid_ind), **record)
    if verbose:
        print(logbook.stream)

    # Begin the generational process
    for gen in range(1, ngen + 1):
        #print("*********Generation %d******************"%gen)
        # Select the next generation individuals
        offspring = toolbox.select(population, len(population))

        # Vary the pool of individuals
        offspring = algorithms.varAnd(offspring, toolbox, cxpb, mutpb)

        # Evaluate the individuals with an invalid fitness
        invalid_ind = [ind for ind in offspring if not ind.fitness.valid]
        fitnesses = toolbox.map(toolbox.evaluate, invalid_ind)
        for ind, fit in zip(invalid_ind, fitnesses):
            ind.fitness.values = fit

        # Update the hall of fame with the generated individuals
        if halloffame is not None:
            halloffame.update(offspring)

        # Replace the current population by the offspring
        population[:] = offspring

        # Append the current generation statistics to the logbook
        record = stats.compile(population) if stats else {}
        logbook.record(gen=gen, nevals=len(invalid_ind), **record)
        if verbose:
            print(logbook.stream)
 #       if stats is not None:
 #           print(record)
            
    return population, logbook

def main():
    global testDataSet
    random.seed(INT_RANDOM_SEED)
    # generate features
    if not REALDATA:
        generator = dG.TDataSource(iSeed=INT_RANDOM_SEED, iNumVar=INT_NUM_VARIABLES,
                               iNumCorr=INT_NUM_CORRELATION_PER_VAR, iNumFeatures=INT_NUM_SAMPLES)
        classification, samples = generator.generateDataset()
        # classification = list(map(lambda x: int(x == True), classification))
        dataSet = DS.TDataSet(samples, classification, RANDOM_SEED=INT_RANDOM_SEED)
        dataSet.splitTrainTestData()
    else:
        _dataSet = DS.TDataSet.fromXLXSFile('alon_data.xlsx', 'alon_data')
        _dataSet.splitTrainTestData(testSize=0.5)
        dataSet = DS.TDataSet(_dataSet.sampleTrain,_dataSet.classTrain, INT_RANDOM_SEED)
        dataSet.splitTrainTestData()
        testDataSet = DS.TDataSet(_dataSet.sampleTest, _dataSet.classTest, INT_RANDOM_SEED)

    # Fitness has 2 goals, maxmize the Classificationrate and minimize the freatures
    creator.create("FitnessMulti", base.Fitness, weights=(-1.0,))
    # each attribute is represented as a bit(boolean) in the list
    creator.create("Individual", list, fitness=creator.FitnessMulti)

    toolbox = base.Toolbox()
    # to generate the initial value, use a function to assign random boolean with probability of
    # having INT_NUM_VARIABLES variables
    toolbox.register("attr_bool", randBool, dProbability=1.0 / INT_NUM_CORRELATION_PER_VAR)
    # initialize each individual using initRepeat (creator.Individual, toolbox.attr_bool, INT_TOTALVARIABLE)
    toolbox.register("individual", tools.initRepeat, creator.Individual, toolbox.attr_bool, n=INT_TOTALVARIABLE)
    toolbox.register("population", tools.initRepeat, list, toolbox.individual)

    toolbox.register("mate", tools.cxTwoPoint)
    toolbox.register("mutate", tools.mutFlipBit, indpb=0.2)
    toolbox.register("select", tools.selTournament, tournsize=3)
    toolbox.register("evaluate", evaluate, _dataSet=dataSet)
    
    # toolbox.register("map", futures.map)
    
    pop = toolbox.population(n=POPULATION_SIZE)
    hof = tools.ParetoFront()
    stats = tools.Statistics(lambda ind: ind.fitness.values)
    stats.register("avg", np.mean, axis=0)
    stats.register("std", np.std, axis=0)
    stats.register("min", np.min, axis=0)
    stats.register("max", np.max, axis=0)
    
    endPop, log = GeneticAlgorithmWithProgress(pop, toolbox, CROSSOVER_PROBABILITY, MUTATE_PROBABILITY, NGEN, stats=stats, halloffame=hof, verbose=True)
    return endPop, stats, hof
    
    

if __name__ == '__main__':
    pop, stats, hof = main()
    print(stats)
    # print(hof)
    idx = np.flatnonzero(hof[0])
    print("With total of %d features for the best performance"%len(idx))
    if not REALDATA:
        random.seed(INT_RANDOM_SEED)
        # generate features
        mgenerator = dG.TDataSource(iSeed=INT_RANDOM_SEED, iNumVar=INT_NUM_VARIABLES,
                               iNumCorr=INT_NUM_CORRELATION_PER_VAR, iNumFeatures=INT_NUM_SAMPLES*2)
        classification, samples = mgenerator.generateDataset()
        # classification = list(map(lambda x: int(x == True), classification))
        mdataSet = DS.TDataSet(samples, classification, RANDOM_SEED=1123)
        mdataSet.splitTrainTestData()
        endFitness = rateCorrectClassification(hof[0], mdataSet)
        print("With the total missclassification of %d in total of %d samples"%(endFitness, INT_NUM_SAMPLES*2))
    else:
        mdataSet = DS.TDataSet.fromXLXSFile(DATA_FILE_NAME, DATA_SHEET_NAME)
        mdataSet.splitTrainTestData()
        testDataSet.splitTrainTestData()
        knn = fitTraindata(hof[0], mdataSet)
        # endFitness = rateCorrectClassification(hof[0], testDataSet)
        endFitness = varifyAttributes(hof[0], testDataSet, knn)
        print("With the total missclassification of %d in total of %d samples"%(endFitness, len(testDataSet.classifications)))
    
    print("Features selected:")
    if REALDATA:
        fileName = DATA_FILE_NAME + "_attributes.txt"
        selectedFeatures = [mdataSet.colNames[i] for i in idx]
        print(selectedFeatures)
    else:
        fileName = "attribute123_%d.txt"%INT_NUM_SAMPLES
        print(idx)
    with open(fileName, 'w') as f:
        for item in idx:
            if REALDATA:
                f.write("%s\n"%(mdataSet.colNames[item]))
            else:
                f.write("%s\n" % item)